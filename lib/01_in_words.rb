class Fixnum
  # Testing why I can't press next. 
 def in_words
   self_num = self
   number = self_num.to_s.chars.map { |n| n.to_i  }
   answer = []

   while number.length > 0
     if self_num / 1000000000000 != 0
       trillions = self_num / 1000000000000
       answer << words(trillions) + ' trillion'
       self_num = number.drop(trillions.to_s.length).join.to_i
       number = number.drop(trillions.to_s.length)
     elsif self_num / 1000000000 != 0
       billions = self_num / 1000000000
       answer << words(billions) + ' billion'
       self_num = number.drop(billions.to_s.length).join.to_i
       number = number.drop(billions.to_s.length)
     elsif self_num / 1000000 != 0
       millions = self_num / 1000000
       answer << words(millions) + ' million'
       self_num = number.drop(millions.to_s.length).join.to_i
       number = number.drop(millions.to_s.length)
     elsif self_num / 1000 != 0
       thousands = self_num / 1000
       answer << words(thousands) + ' thousand'
       self_num = number.drop(thousands.to_s.length).join.to_i
       number = number.drop(thousands.to_s.length)
     else
       break if number.all? { |el| el == 0 } && number.length > 1
       answer << words(self_num)
       self_num = number.drop(self_num).join.to_i
       number = number.drop(self_num)
       break
     end
     answer
   end

   answer.join(' ')
 end

 private

 def words(numbers)
   number = numbers.to_s.chars.map { |n| n.to_i  }
   result = []
   number.each_with_index do |num, i|
     if number[i..-1].length == 1
       break if number.length > 1 && num == 0
       result << single_digit(num)
     elsif number[i..-1].length == 2 && number[i] == 1
       result << ten_to_nineteen(number[i..-1].join('').to_i)
       break
     elsif number[i..-1].length == 2 && number[i] > 1
       result << tens_digits(num)
     elsif number[i..-1].length == 3
       result << single_digit(num) + ' hundred'
     end
     result
   end
   result.join(' ').strip
 end

 def single_digit(num)
   case num
   when 0
     return 'zero'
   when 1
     return 'one'
   when 2
     return 'two'
   when 3
     return 'three'
   when 4
     return 'four'
   when 5
     return 'five'
   when 6
     return 'six'
   when 7
     return 'seven'
   when 8
     return 'eight'
   when 9
     return 'nine'
   end
 end

 def ten_to_nineteen(num)
   case num
   when 10
     return 'ten'
   when 11
     return 'eleven'
   when 12
     return 'twelve'
   when 13
     return 'thirteen'
   when 14
     return 'fourteen'
   when 15
     return 'fifteen'
   when 16
     return 'sixteen'
   when 17
     return 'seventeen'
   when 18
     return 'eighteen'
   when 19
     return 'nineteen'
   end
 end

 def tens_digits(num)
   case num
   when 2
     return 'twenty'
   when 3
     return 'thirty'
   when 4
     return 'forty'
   when 5
     return 'fifty'
   when 6
     return 'sixty'
   when 7
     return 'seventy'
   when 8
     return 'eighty'
   when 9
     return 'ninety'
   end
 end
end
